package katas.gildedrose;

import io.vavr.collection.LinkedHashMap;
import io.vavr.collection.Map;
import katas.gildedrose.action.ItemAction;

import static io.vavr.Tuple.of;
import static java.util.Arrays.asList;
import static katas.gildedrose.action.ItemActions.*;
import static katas.gildedrose.action.ItemPredicates.expireInLessThan;
import static katas.gildedrose.action.ItemPredicates.hasExpired;

public class GildedRose {
    public static final ItemAction DEFAULT = sequence(
            decreaseQuality(1),
            decreaseSellIn(1),
            when(hasExpired(), decreaseQuality(1)));
    public static final Map<String, ItemAction> RULES = LinkedHashMap.ofEntries(asList(
            of("Aged Brie", sequence(
                    increaseQuality(1),
                    decreaseSellIn(1),
                    when(hasExpired(), increaseQuality(1)))),
            of("Sulfuras, Hand of Ragnaros", sequence()),
            of("Backstage passes to a TAFKAL80ETC concert", sequence(
                    increaseQuality(1),
                    when(expireInLessThan(11), increaseQuality(1)),
                    when(expireInLessThan(6), increaseQuality(1)),
                    decreaseSellIn(1),
                    when(hasExpired(), setQuality(0))))
    ));


    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
            ruleFor(item).apply(item);
        }
    }

    private ItemAction ruleFor(final Item item) {
        return RULES.getOrElse(item.name, DEFAULT);
    }
}

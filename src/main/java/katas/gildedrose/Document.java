package katas.gildedrose;

import io.vavr.Tuple2;
import katas.gildedrose.action.ActionDescription;
import katas.gildedrose.action.ItemAction;

public class Document {

    private static String describe(final String name, ItemAction action) {
        final ActionDescription description = new ActionDescription();
        action.describe(description);
        return "# " + name + "\n\n" + description.render() + "\n";
    }

    public static void main(String[] args) {
        for (Tuple2<String, ItemAction> entry : GildedRose.RULES) {
            System.out.println(describe(entry._1, entry._2));
        }
        System.out.println(describe("default", GildedRose.DEFAULT));
    }
}

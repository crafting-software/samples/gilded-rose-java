package katas.gildedrose.action;

import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.List;

public final class ActionDescription {
    private final List<String> parts = new ArrayList<>();

    public void action(String value) {
        parts.add(value);
    }

    public void condition(String value) {
        parts.add(value);
    }

    public String render() {
        return Joiner.on(" ").join(parts);
    }

    public void add(String value) {
        parts.add(value);
    }
}

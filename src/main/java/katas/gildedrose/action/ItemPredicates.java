package katas.gildedrose.action;

import katas.gildedrose.Item;

public final class ItemPredicates {
    public static ItemPredicate hasExpired() {
        return new ItemPredicate() {
            public boolean check(Item item) {
                return item.sellIn < 0;
            }

            public void describe(ActionDescription description) {
                description.condition("has expired");
            }
        };
    }

    public static ItemPredicate expireInLessThan(int days) {
        return new ItemPredicate() {
            public boolean check(Item item) {
                return item.sellIn < days;
            }

            public void describe(ActionDescription description) {
                description.condition("expired before " + days + " days");
            }
        };
    }
}

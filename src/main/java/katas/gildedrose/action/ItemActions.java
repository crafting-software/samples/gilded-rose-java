package katas.gildedrose.action;

import katas.gildedrose.Item;

import java.util.Arrays;
import java.util.List;

public final class ItemActions {

    public static ItemAction sequence(final ItemAction... actions) {
        return sequence(Arrays.asList(actions));
    }

    public static ItemAction sequence(final List<ItemAction> actions) {
        return new ItemAction() {
            public void apply(Item item) {
                for (ItemAction action : actions) {
                    action.apply(item);
                }
            }

            public void describe(ActionDescription description) {
                for (ItemAction action : actions) {
                    action.describe(description);
                }
            }
        };
    }

    public static ItemAction when(final ItemPredicate predicate, final ItemAction action) {
        return new ItemAction() {
            public void apply(Item item) {
                if (predicate.check(item)) {
                    action.apply(item);
                }
            }

            public void describe(ActionDescription description) {
                description.add("if (");
                predicate.describe(description);
                description.add(")");
                description.add("{");
                action.describe(description);
                description.add("}");
            }
        };
    }

    public static ItemAction increaseQuality(final int value) {
        if (value < 0) {
            throw new IllegalArgumentException("cannot increase quality with a negative value");
        }
        return new ItemAction() {
            public void apply(Item item) {
                item.quality = Math.min(50, item.quality + value);
            }

            public void describe(ActionDescription description) {
                description.action("quality += " + value + ";");
            }
        };
    }

    public static ItemAction decreaseQuality(final int value) {
        if (value < 0) {
            throw new IllegalArgumentException("cannot decrease quality with a negative value");
        }
        return new ItemAction() {
            public void apply(Item item) {
                item.quality = Math.max(0, item.quality - value);
            }

            public void describe(ActionDescription description) {
                description.action("quality -= " + value + ";");
            }
        };
    }

    public static ItemAction setQuality(final int value) {
        if (value < 0) {
            throw new IllegalArgumentException("cannot set quality with a negative value");
        }
        if (value > 50) {
            throw new IllegalArgumentException("cannot set quality with a value beyond 50");
        }
        return new ItemAction() {
            public void apply(Item item) {
                item.quality = value;
            }

            public void describe(ActionDescription description) {
                description.action("quality = " + value + ";");
            }
        };
    }

    public static ItemAction decreaseSellIn(final int value) {
        if (value < 0) {
            throw new IllegalArgumentException("cannot decrease sell in with a negative value");
        }
        return new ItemAction() {
            public void apply(Item item) {
                item.sellIn -= value;
            }

            public void describe(ActionDescription description) {
                description.action("sell in -= " + value + ";");
            }
        };
    }
}

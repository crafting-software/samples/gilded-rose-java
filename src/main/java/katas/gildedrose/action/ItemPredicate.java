package katas.gildedrose.action;

import katas.gildedrose.Item;

public interface ItemPredicate {
    boolean check(final Item item);

    void describe(ActionDescription description);
}

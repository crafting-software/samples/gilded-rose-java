package katas.gildedrose.action;

import katas.gildedrose.Item;

public interface ItemAction {
    void apply(Item item); // side effect

    void describe(ActionDescription description);
}

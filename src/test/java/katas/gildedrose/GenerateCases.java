package katas.gildedrose;

import com.google.common.io.Files;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupString;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public final class GenerateCases {
    private static String TEMPLATES = """
            method(case) ::= <<
                @Test
                void test_<case.number>() {
                     Item[] items = new Item[] { new Item("<case.name>", <case.sellIn>, <case.quality>) };
                    final GildedRose app = new GildedRose(items);
                    app.updateQuality();
                    assertEquals(<case.expectedSellIn>, items[0].sellIn);
                    assertEquals(<case.expectedQuality>, items[0].quality);
                }

            >>

            full(cases) ::= <<
            package katas.gildedrose;
                        
            import katas.gildedrose.GildedRose;
            import katas.gildedrose.Item;
            import org.junit.jupiter.api.Test;
                        
            import static org.junit.jupiter.api.Assertions.assertEquals;

            class GeneratedUnitTest {
                <cases : method()>
            }
            >>

                """;

    private static final Random RANDOM = new Random();
    private static final List<String> NAMES = Arrays.asList(
            "+5 Dexterity Vest",
            "Aged Brie",
            "Elixir of the Mongoose",
            "Sulfuras, Hand of Ragnaros",
            "Backstage passes to a TAFKAL80ETC concert");


    private static int quality() {
        return Math.abs(RANDOM.nextInt()) % 51;
    }

    private static int sellIn() {
        return Math.abs(RANDOM.nextInt()) % 200 - 100;
    }

    private static String name() {
        return NAMES.get(RANDOM.nextInt(NAMES.size()));
    }

    private static Item item() {
        return new Item(name(), sellIn(), quality());
    }

    static class Case {
        public int number;
        public String name;
        public int sellIn;
        public int quality;
        public int expectedSellIn = 0;
        public int expectedQuality = 0;

        Case(int number, String name, int sellIn, int quality) {
            this.number = number;
            this.name = name;
            this.sellIn = sellIn;
            this.quality = quality;
        }
    }

    private static Case buildCase(final int number) {
        final Item item = item();
        Case result = new Case(number, item.name, item.sellIn, item.quality);
        final GildedRose app = new GildedRose(new Item[]{item});
        app.updateQuality();
        result.expectedQuality = item.quality;
        result.expectedSellIn = item.sellIn;
        return result;
    }

    public static void main(String[] args) throws IOException {
        List<Case> cases = new ArrayList<>();
        for (int i = 0; i < 1000; ++i) {
            cases.add(buildCase(i));
        }
        STGroup templates = new STGroupString(TEMPLATES);
        ST st = templates.getInstanceOf("full");
        st.add("cases", cases);
        Files.asCharSink(new File("src/test/java/katas/gildedrose/GeneratedUnitTest.java"), StandardCharsets.UTF_8).write(st.render());
    }
}

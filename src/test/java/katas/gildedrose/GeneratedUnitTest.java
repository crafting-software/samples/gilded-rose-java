package katas.gildedrose;

import katas.gildedrose.GildedRose;
import katas.gildedrose.Item;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GeneratedUnitTest {
        @Test
        void test_0() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -37, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-37, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_1() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 31, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(30, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_2() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -35, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-36, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_3() {
             Item[] items = new Item[] { new Item("Aged Brie", 69, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_4() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -24, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-25, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_5() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -77, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-78, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_6() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -42, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_7() {
             Item[] items = new Item[] { new Item("Aged Brie", 74, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(73, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_8() {
             Item[] items = new Item[] { new Item("Aged Brie", 30, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(29, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_9() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(4, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_10() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -67, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-68, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_11() {
             Item[] items = new Item[] { new Item("Aged Brie", 55, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(54, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_12() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -17, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-18, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_13() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 28, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(27, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_14() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -81, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-81, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_15() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 68, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_16() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 83, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(82, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_17() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 34, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(33, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_18() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -47, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-48, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_19() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 77, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(77, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_20() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 12, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(11, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_21() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 16, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(15, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_22() {
             Item[] items = new Item[] { new Item("Aged Brie", -48, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-49, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_23() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 84, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(83, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_24() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 93, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(92, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_25() {
             Item[] items = new Item[] { new Item("Aged Brie", 76, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_26() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -65, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_27() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -90, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-90, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_28() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 76, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_29() {
             Item[] items = new Item[] { new Item("Aged Brie", 44, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_30() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 97, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_31() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 66, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(65, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_32() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -20, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-21, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_33() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 32, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(31, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_34() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 71, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(70, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_35() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 17, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(17, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_36() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -21, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-22, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_37() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -92, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-93, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_38() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 92, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(91, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_39() {
             Item[] items = new Item[] { new Item("Aged Brie", -28, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-29, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_40() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -31, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-32, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_41() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -27, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-28, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_42() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -57, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-58, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_43() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -44, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-45, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_44() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 37, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(36, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_45() {
             Item[] items = new Item[] { new Item("Aged Brie", -91, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-92, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_46() {
             Item[] items = new Item[] { new Item("Aged Brie", -85, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-86, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_47() {
             Item[] items = new Item[] { new Item("Aged Brie", 1, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_48() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 44, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(44, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_49() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 44, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_50() {
             Item[] items = new Item[] { new Item("Aged Brie", -91, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-92, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_51() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -95, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-96, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_52() {
             Item[] items = new Item[] { new Item("Aged Brie", 31, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(30, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_53() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -71, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-72, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_54() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 83, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(82, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_55() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -25, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-26, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_56() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 8, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(7, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_57() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -31, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-31, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_58() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-1, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_59() {
             Item[] items = new Item[] { new Item("Aged Brie", -68, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-69, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_60() {
             Item[] items = new Item[] { new Item("Aged Brie", 59, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(58, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_61() {
             Item[] items = new Item[] { new Item("Aged Brie", -70, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-71, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_62() {
             Item[] items = new Item[] { new Item("Aged Brie", -96, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-97, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_63() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 97, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_64() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -59, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-60, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_65() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 17, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_66() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 94, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(93, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_67() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -14, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-15, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_68() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -29, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-30, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_69() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 41, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(41, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_70() {
             Item[] items = new Item[] { new Item("Aged Brie", -64, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-65, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_71() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -83, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-83, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_72() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -65, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-65, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_73() {
             Item[] items = new Item[] { new Item("Aged Brie", -26, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-27, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_74() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 49, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_75() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 96, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(95, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_76() {
             Item[] items = new Item[] { new Item("Aged Brie", 79, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(78, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_77() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 89, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_78() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 95, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(95, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_79() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 96, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(95, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_80() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -84, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_81() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -66, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_82() {
             Item[] items = new Item[] { new Item("Aged Brie", -14, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-15, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_83() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 90, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(90, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_84() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 3, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(2, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_85() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 41, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(40, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_86() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -20, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_87() {
             Item[] items = new Item[] { new Item("Aged Brie", 97, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_88() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -57, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-58, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_89() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 21, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(20, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_90() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 98, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(97, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_91() {
             Item[] items = new Item[] { new Item("Aged Brie", 41, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(40, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_92() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 78, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(77, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_93() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -9, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-10, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_94() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -59, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-60, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_95() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 70, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(70, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_96() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -65, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_97() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 66, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(65, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_98() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 80, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(79, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_99() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -37, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-37, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_100() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 73, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(72, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_101() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -85, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-86, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_102() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 36, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(35, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_103() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 86, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(86, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_104() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 66, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_105() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -27, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-27, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_106() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -20, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-21, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_107() {
             Item[] items = new Item[] { new Item("Aged Brie", 52, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_108() {
             Item[] items = new Item[] { new Item("Aged Brie", -100, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-101, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_109() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 87, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(86, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_110() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 95, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(94, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_111() {
             Item[] items = new Item[] { new Item("Aged Brie", 32, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(31, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_112() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(9, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_113() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -60, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-61, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_114() {
             Item[] items = new Item[] { new Item("Aged Brie", 8, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(7, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_115() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 78, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(77, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_116() {
             Item[] items = new Item[] { new Item("Aged Brie", 49, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_117() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 29, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(28, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_118() {
             Item[] items = new Item[] { new Item("Aged Brie", 82, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(81, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_119() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -3, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_120() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -1, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-2, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_121() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 68, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(67, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_122() {
             Item[] items = new Item[] { new Item("Aged Brie", -51, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-52, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_123() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 7, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(6, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_124() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -34, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-35, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_125() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -31, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-32, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_126() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -50, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-50, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_127() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -94, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_128() {
             Item[] items = new Item[] { new Item("Aged Brie", -88, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-89, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_129() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 74, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(74, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_130() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 65, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(65, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_131() {
             Item[] items = new Item[] { new Item("Aged Brie", -93, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_132() {
             Item[] items = new Item[] { new Item("Aged Brie", -21, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-22, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_133() {
             Item[] items = new Item[] { new Item("Aged Brie", -40, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-41, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_134() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -7, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_135() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -7, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-8, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_136() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -21, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-22, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_137() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 87, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(86, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_138() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 28, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(27, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_139() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -7, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_140() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -61, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-62, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_141() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 68, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(67, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_142() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 94, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(93, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_143() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -21, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-22, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_144() {
             Item[] items = new Item[] { new Item("Aged Brie", 49, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_145() {
             Item[] items = new Item[] { new Item("Aged Brie", 11, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(10, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_146() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -51, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-52, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_147() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -84, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_148() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -80, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-81, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_149() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 60, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_150() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 51, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_151() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -69, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-69, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_152() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 31, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(30, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_153() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -37, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-38, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_154() {
             Item[] items = new Item[] { new Item("Aged Brie", -92, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-93, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_155() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 93, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(92, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_156() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -43, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_157() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 57, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(56, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_158() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 52, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_159() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -84, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_160() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 49, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_161() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -50, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-50, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_162() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 95, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(94, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_163() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -89, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-90, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_164() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -58, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-58, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_165() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 70, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(70, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_166() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 40, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(40, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_167() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -8, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_168() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -76, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_169() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -62, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-62, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_170() {
             Item[] items = new Item[] { new Item("Aged Brie", -72, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-73, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_171() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 6, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(5, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_172() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 39, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_173() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 57, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(56, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_174() {
             Item[] items = new Item[] { new Item("Aged Brie", 11, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(10, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_175() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -46, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-47, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_176() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 42, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(42, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_177() {
             Item[] items = new Item[] { new Item("Aged Brie", 26, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(25, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_178() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -91, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-91, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_179() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 11, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(10, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_180() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -73, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-74, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_181() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 54, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(53, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_182() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -69, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-70, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_183() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -40, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-41, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_184() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -16, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-17, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_185() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -72, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-73, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_186() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 5, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(4, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_187() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 20, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(19, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_188() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 52, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_189() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -91, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-92, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_190() {
             Item[] items = new Item[] { new Item("Aged Brie", 41, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(40, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_191() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -43, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_192() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -19, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_193() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 99, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_194() {
             Item[] items = new Item[] { new Item("Aged Brie", 84, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(83, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_195() {
             Item[] items = new Item[] { new Item("Aged Brie", 17, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_196() {
             Item[] items = new Item[] { new Item("Aged Brie", 19, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(18, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_197() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -34, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-35, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_198() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -65, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_199() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 80, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(80, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_200() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 47, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(46, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_201() {
             Item[] items = new Item[] { new Item("Aged Brie", 98, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(97, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_202() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 40, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(40, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_203() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 35, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(34, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_204() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 63, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(63, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_205() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 67, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_206() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 22, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_207() {
             Item[] items = new Item[] { new Item("Aged Brie", 44, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_208() {
             Item[] items = new Item[] { new Item("Aged Brie", 36, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(35, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_209() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -29, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-30, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_210() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -28, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-29, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_211() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -65, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_212() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 52, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_213() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -5, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-6, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_214() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -66, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-67, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_215() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(14, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_216() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 35, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(34, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_217() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -52, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-53, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_218() {
             Item[] items = new Item[] { new Item("Aged Brie", 38, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(37, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_219() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -6, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_220() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -11, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-12, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_221() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 21, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(20, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_222() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 41, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(40, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_223() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -89, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-89, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_224() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 29, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(28, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_225() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 2, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(1, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_226() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -58, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-59, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_227() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -91, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-92, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_228() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 97, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(97, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_229() {
             Item[] items = new Item[] { new Item("Aged Brie", 7, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(6, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_230() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 88, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(87, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_231() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 64, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(63, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_232() {
             Item[] items = new Item[] { new Item("Aged Brie", 17, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_233() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 69, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(69, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_234() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 60, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(60, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_235() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 76, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_236() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 99, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(99, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_237() {
             Item[] items = new Item[] { new Item("Aged Brie", 39, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_238() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 1, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_239() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -93, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_240() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 11, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(10, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_241() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 96, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_242() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 1, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_243() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -89, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-90, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_244() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -80, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-81, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_245() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -45, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-46, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_246() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 60, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_247() {
             Item[] items = new Item[] { new Item("Aged Brie", -86, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-87, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_248() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 83, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(82, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_249() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 19, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(19, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_250() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -57, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-58, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_251() {
             Item[] items = new Item[] { new Item("Aged Brie", 84, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(83, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_252() {
             Item[] items = new Item[] { new Item("Aged Brie", -7, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-8, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_253() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -24, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-25, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_254() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -11, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-11, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_255() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 59, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_256() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -4, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-5, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_257() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 86, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_258() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 47, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(46, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_259() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 82, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(82, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_260() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -5, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-6, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_261() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 73, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(72, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_262() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 14, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(13, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_263() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -96, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-97, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_264() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -75, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-75, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_265() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -45, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-46, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_266() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -92, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-93, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_267() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 13, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(12, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_268() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -84, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_269() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 6, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(5, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_270() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -33, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-34, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_271() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 92, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(91, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_272() {
             Item[] items = new Item[] { new Item("Aged Brie", -20, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-21, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_273() {
             Item[] items = new Item[] { new Item("Aged Brie", -64, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-65, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_274() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 77, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(76, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_275() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 80, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(79, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_276() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -22, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-23, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_277() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 3, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(2, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_278() {
             Item[] items = new Item[] { new Item("Aged Brie", -11, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-12, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_279() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 39, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_280() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 40, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_281() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 51, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_282() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 16, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(15, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_283() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 11, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(10, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_284() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -63, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-64, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_285() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 92, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(91, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_286() {
             Item[] items = new Item[] { new Item("Aged Brie", 19, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(18, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_287() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 36, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(35, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_288() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -48, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-49, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_289() {
             Item[] items = new Item[] { new Item("Aged Brie", 2, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(1, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_290() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -78, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-79, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_291() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 65, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(64, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_292() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -93, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_293() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 20, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(19, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_294() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 52, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_295() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 49, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_296() {
             Item[] items = new Item[] { new Item("Aged Brie", 40, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_297() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 85, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_298() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 64, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(63, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_299() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 36, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(35, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_300() {
             Item[] items = new Item[] { new Item("Aged Brie", 50, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(49, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_301() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 1, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_302() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 92, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(92, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_303() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 82, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(81, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_304() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 22, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_305() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 61, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(61, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_306() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -2, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-3, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_307() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -92, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-93, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_308() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -19, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_309() {
             Item[] items = new Item[] { new Item("Aged Brie", 19, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(18, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_310() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 8, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(7, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_311() {
             Item[] items = new Item[] { new Item("Aged Brie", 69, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_312() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 87, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(86, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_313() {
             Item[] items = new Item[] { new Item("Aged Brie", -27, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-28, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_314() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -87, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-88, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_315() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 65, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(65, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_316() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 3, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(2, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_317() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -79, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-80, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_318() {
             Item[] items = new Item[] { new Item("Aged Brie", -20, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-21, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_319() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 74, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(73, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_320() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -86, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-86, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_321() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -39, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-40, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_322() {
             Item[] items = new Item[] { new Item("Aged Brie", 51, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_323() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 9, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(8, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_324() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 0, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_325() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -24, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-25, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_326() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -96, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-96, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_327() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -18, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-18, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_328() {
             Item[] items = new Item[] { new Item("Aged Brie", -11, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-12, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_329() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 7, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(6, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_330() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 88, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(87, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_331() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 43, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(42, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_332() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -99, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-100, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_333() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -39, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-40, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_334() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 1, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_335() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 90, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_336() {
             Item[] items = new Item[] { new Item("Aged Brie", 34, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(33, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_337() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -61, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-62, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_338() {
             Item[] items = new Item[] { new Item("Aged Brie", -69, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-70, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_339() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -54, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-55, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_340() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -2, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-3, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_341() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 99, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_342() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 47, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(46, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_343() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 73, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(73, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_344() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -7, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-8, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_345() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 63, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(62, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_346() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -10, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-10, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_347() {
             Item[] items = new Item[] { new Item("Aged Brie", -17, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-18, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_348() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -47, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-47, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_349() {
             Item[] items = new Item[] { new Item("Aged Brie", 33, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(32, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_350() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -80, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-81, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_351() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(10, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_352() {
             Item[] items = new Item[] { new Item("Aged Brie", -86, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-87, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_353() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 40, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_354() {
             Item[] items = new Item[] { new Item("Aged Brie", 26, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(25, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_355() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 22, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_356() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -66, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-67, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_357() {
             Item[] items = new Item[] { new Item("Aged Brie", 3, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(2, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_358() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -59, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-60, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_359() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 91, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(90, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_360() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 90, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_361() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 39, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_362() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 28, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(27, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_363() {
             Item[] items = new Item[] { new Item("Aged Brie", -62, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-63, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_364() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -54, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-55, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_365() {
             Item[] items = new Item[] { new Item("Aged Brie", -40, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-41, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_366() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -82, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-82, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_367() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 16, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_368() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 31, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(30, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_369() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -23, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-24, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_370() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -90, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-91, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_371() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 51, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_372() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -8, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_373() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 34, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(34, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_374() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -39, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-40, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_375() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -46, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-47, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_376() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 89, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_377() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 57, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(57, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_378() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 85, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(84, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_379() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 68, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_380() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -94, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_381() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -37, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-37, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_382() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -24, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-24, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_383() {
             Item[] items = new Item[] { new Item("Aged Brie", -48, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-49, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_384() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 18, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(17, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_385() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 31, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(30, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_386() {
             Item[] items = new Item[] { new Item("Aged Brie", 39, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_387() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 99, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_388() {
             Item[] items = new Item[] { new Item("Aged Brie", -2, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-3, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_389() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -28, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-29, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_390() {
             Item[] items = new Item[] { new Item("Aged Brie", 37, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(36, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_391() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 21, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_392() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -89, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-90, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_393() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 95, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(94, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_394() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -60, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-61, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_395() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 63, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(62, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_396() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -92, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-92, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_397() {
             Item[] items = new Item[] { new Item("Aged Brie", -84, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_398() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 76, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_399() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 80, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(79, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_400() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -35, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-36, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_401() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -7, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_402() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 59, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(58, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_403() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -7, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_404() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 52, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_405() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 44, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_406() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -29, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-30, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_407() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 49, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_408() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 40, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_409() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 70, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(70, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_410() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -28, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-29, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_411() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -66, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-67, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_412() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -34, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-34, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_413() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -20, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-21, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_414() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -42, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_415() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 48, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(47, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_416() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -9, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-10, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_417() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 90, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_418() {
             Item[] items = new Item[] { new Item("Aged Brie", 6, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(5, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_419() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -24, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-25, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_420() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-1, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_421() {
             Item[] items = new Item[] { new Item("Aged Brie", 92, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(91, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_422() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 78, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(77, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_423() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -97, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-98, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_424() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 67, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_425() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 90, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_426() {
             Item[] items = new Item[] { new Item("Aged Brie", -17, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-18, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_427() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -36, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-36, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_428() {
             Item[] items = new Item[] { new Item("Aged Brie", 84, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(83, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_429() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 21, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(20, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_430() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -77, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-78, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_431() {
             Item[] items = new Item[] { new Item("Aged Brie", 15, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(14, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_432() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -85, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_433() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 28, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(27, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_434() {
             Item[] items = new Item[] { new Item("Aged Brie", 2, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(1, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_435() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -14, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-14, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_436() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -94, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_437() {
             Item[] items = new Item[] { new Item("Aged Brie", -68, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-69, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_438() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -83, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-84, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_439() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -96, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-97, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_440() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -11, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-12, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_441() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -32, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-32, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_442() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 76, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_443() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -60, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-61, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_444() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -94, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_445() {
             Item[] items = new Item[] { new Item("Aged Brie", -12, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-13, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_446() {
             Item[] items = new Item[] { new Item("Aged Brie", -40, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-41, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_447() {
             Item[] items = new Item[] { new Item("Aged Brie", 51, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_448() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 24, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(23, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_449() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -4, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-5, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_450() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -51, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-52, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_451() {
             Item[] items = new Item[] { new Item("Aged Brie", 22, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_452() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -3, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_453() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 53, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(52, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_454() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 62, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(61, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_455() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 65, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(64, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_456() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -12, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-13, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_457() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -62, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-63, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_458() {
             Item[] items = new Item[] { new Item("Aged Brie", 49, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_459() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -16, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-17, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_460() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 39, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_461() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -42, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_462() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 13, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(13, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_463() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 20, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(19, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_464() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -47, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-47, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_465() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 52, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_466() {
             Item[] items = new Item[] { new Item("Aged Brie", 83, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(82, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_467() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -63, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-64, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_468() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 97, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_469() {
             Item[] items = new Item[] { new Item("Aged Brie", -72, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-73, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_470() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 47, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(46, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_471() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -29, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-30, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_472() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 43, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_473() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -91, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-91, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_474() {
             Item[] items = new Item[] { new Item("Aged Brie", 99, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_475() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -47, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-48, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_476() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 23, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(23, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_477() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 93, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(92, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_478() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -72, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-73, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_479() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 23, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(22, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_480() {
             Item[] items = new Item[] { new Item("Aged Brie", -94, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_481() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -93, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_482() {
             Item[] items = new Item[] { new Item("Aged Brie", -54, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-55, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_483() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 12, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(11, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_484() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -92, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-93, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_485() {
             Item[] items = new Item[] { new Item("Aged Brie", 31, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(30, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_486() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -27, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-27, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_487() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 4, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(3, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_488() {
             Item[] items = new Item[] { new Item("Aged Brie", -91, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-92, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_489() {
             Item[] items = new Item[] { new Item("Aged Brie", 43, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(42, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_490() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 83, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(83, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_491() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 84, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(84, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_492() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -65, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_493() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 20, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(19, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_494() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -44, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-45, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_495() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 80, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(79, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_496() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 34, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(33, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_497() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -39, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-40, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_498() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -62, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-63, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_499() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -43, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_500() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 81, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(81, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_501() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -94, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_502() {
             Item[] items = new Item[] { new Item("Aged Brie", 76, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_503() {
             Item[] items = new Item[] { new Item("Aged Brie", -59, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-60, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_504() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 88, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(87, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_505() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 48, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(47, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_506() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 37, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(36, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_507() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 73, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(72, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_508() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 92, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(91, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_509() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 87, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(87, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_510() {
             Item[] items = new Item[] { new Item("Aged Brie", 20, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(19, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_511() {
             Item[] items = new Item[] { new Item("Aged Brie", 64, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(63, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_512() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 16, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_513() {
             Item[] items = new Item[] { new Item("Aged Brie", 7, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(6, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_514() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 39, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_515() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 20, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(19, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_516() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 69, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_517() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 59, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(58, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_518() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -53, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-54, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_519() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 63, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(62, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_520() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 13, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(13, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_521() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -98, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-99, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_522() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -62, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-62, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_523() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -47, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-48, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_524() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -84, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_525() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -35, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-36, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_526() {
             Item[] items = new Item[] { new Item("Aged Brie", -83, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-84, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_527() {
             Item[] items = new Item[] { new Item("Aged Brie", 25, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(24, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_528() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -34, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-35, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_529() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -6, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_530() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -4, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_531() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -7, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_532() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -19, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_533() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -87, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-88, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_534() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -53, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-54, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_535() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 89, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_536() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 81, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(80, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_537() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 97, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_538() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 90, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(90, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_539() {
             Item[] items = new Item[] { new Item("Aged Brie", -68, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-69, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_540() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -38, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-39, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_541() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -25, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-26, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_542() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 21, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(20, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_543() {
             Item[] items = new Item[] { new Item("Aged Brie", -42, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_544() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -96, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-97, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_545() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 25, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(24, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_546() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 95, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(94, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_547() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 43, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(42, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_548() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 31, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(30, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_549() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -89, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-89, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_550() {
             Item[] items = new Item[] { new Item("Aged Brie", -63, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-64, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_551() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 86, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_552() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 11, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(10, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_553() {
             Item[] items = new Item[] { new Item("Aged Brie", 54, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(53, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_554() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -10, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-11, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_555() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 87, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(86, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_556() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -19, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_557() {
             Item[] items = new Item[] { new Item("Aged Brie", 89, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_558() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -48, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-49, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_559() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -95, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-96, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_560() {
             Item[] items = new Item[] { new Item("Aged Brie", -76, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_561() {
             Item[] items = new Item[] { new Item("Aged Brie", 25, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(24, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_562() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -29, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-30, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_563() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 19, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(18, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_564() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -49, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-50, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_565() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -74, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-75, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_566() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 40, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_567() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 7, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(6, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_568() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 46, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(45, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_569() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 97, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_570() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -54, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-55, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_571() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 93, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(93, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_572() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 0, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-1, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_573() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 44, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_574() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 29, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(28, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_575() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -94, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_576() {
             Item[] items = new Item[] { new Item("Aged Brie", 30, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(29, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_577() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -4, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-5, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_578() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -36, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-36, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_579() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -57, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-58, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_580() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 96, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(95, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_581() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -13, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-14, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_582() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -3, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_583() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 75, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(74, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_584() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -100, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-101, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_585() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 47, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(46, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_586() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -61, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-62, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_587() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 70, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(69, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_588() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 50, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_589() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 40, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_590() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -4, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_591() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -32, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-33, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_592() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 53, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(52, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_593() {
             Item[] items = new Item[] { new Item("Aged Brie", 96, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(95, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_594() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 68, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(67, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_595() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -66, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-67, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_596() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -52, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-53, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_597() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -39, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-40, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_598() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 78, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(77, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_599() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -44, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-44, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_600() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -23, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-24, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_601() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -50, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-51, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_602() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 39, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_603() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -11, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-12, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_604() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 17, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_605() {
             Item[] items = new Item[] { new Item("Aged Brie", -41, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-42, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_606() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 89, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_607() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 13, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(12, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_608() {
             Item[] items = new Item[] { new Item("Aged Brie", 25, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(24, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_609() {
             Item[] items = new Item[] { new Item("Aged Brie", 42, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(41, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_610() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 17, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_611() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 59, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_612() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -95, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-96, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_613() {
             Item[] items = new Item[] { new Item("Aged Brie", -36, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-37, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_614() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 42, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(41, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_615() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 49, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_616() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 42, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(42, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_617() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -71, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-72, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_618() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -88, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-89, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_619() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 5, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(5, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_620() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -44, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-45, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_621() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -69, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-70, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_622() {
             Item[] items = new Item[] { new Item("Aged Brie", 56, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(55, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_623() {
             Item[] items = new Item[] { new Item("Aged Brie", -60, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-61, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_624() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 44, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_625() {
             Item[] items = new Item[] { new Item("Aged Brie", 10, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(9, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_626() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -88, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-89, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_627() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 53, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(52, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_628() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 19, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(18, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_629() {
             Item[] items = new Item[] { new Item("Aged Brie", 69, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_630() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 81, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(80, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_631() {
             Item[] items = new Item[] { new Item("Aged Brie", -21, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-22, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_632() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -95, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-96, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_633() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -78, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-78, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_634() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -65, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_635() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 29, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(28, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_636() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -82, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-83, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_637() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -85, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-86, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_638() {
             Item[] items = new Item[] { new Item("Aged Brie", -95, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-96, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_639() {
             Item[] items = new Item[] { new Item("Aged Brie", 53, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(52, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_640() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 95, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(95, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_641() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -72, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-72, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_642() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -23, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-24, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_643() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 88, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(87, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_644() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 76, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_645() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 28, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(28, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_646() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 89, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_647() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 67, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_648() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -8, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_649() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 97, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_650() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 40, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_651() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -85, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_652() {
             Item[] items = new Item[] { new Item("Aged Brie", -42, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_653() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -8, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-8, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_654() {
             Item[] items = new Item[] { new Item("Aged Brie", 6, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(5, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_655() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 95, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(94, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_656() {
             Item[] items = new Item[] { new Item("Aged Brie", 34, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(33, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_657() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -71, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-72, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_658() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -83, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-83, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_659() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 58, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(57, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_660() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -76, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_661() {
             Item[] items = new Item[] { new Item("Aged Brie", -15, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-16, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_662() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -69, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-70, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_663() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 26, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(25, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_664() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -27, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-28, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_665() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 44, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(44, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_666() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 1, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_667() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 76, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_668() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 67, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_669() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 43, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_670() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -68, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-69, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_671() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 49, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_672() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -97, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-98, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_673() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -29, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-30, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_674() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 96, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_675() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -58, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-59, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_676() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -30, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-31, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_677() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 54, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(53, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_678() {
             Item[] items = new Item[] { new Item("Aged Brie", 56, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(55, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_679() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 22, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_680() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 86, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_681() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 53, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(52, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_682() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 29, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(29, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_683() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 77, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(77, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_684() {
             Item[] items = new Item[] { new Item("Aged Brie", -20, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-21, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_685() {
             Item[] items = new Item[] { new Item("Aged Brie", 17, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_686() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -45, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-45, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_687() {
             Item[] items = new Item[] { new Item("Aged Brie", -40, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-41, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_688() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -1, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-1, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_689() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -85, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-86, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_690() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 11, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(11, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_691() {
             Item[] items = new Item[] { new Item("Aged Brie", -22, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-23, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_692() {
             Item[] items = new Item[] { new Item("Aged Brie", -94, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_693() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 22, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_694() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 54, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(53, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_695() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 4, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(3, items[0].sellIn);
            assertEquals(20, items[0].quality);
        }
        @Test
        void test_696() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 50, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_697() {
             Item[] items = new Item[] { new Item("Aged Brie", -12, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-13, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_698() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -33, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-34, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_699() {
             Item[] items = new Item[] { new Item("Aged Brie", -48, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-49, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_700() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -77, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_701() {
             Item[] items = new Item[] { new Item("Aged Brie", -66, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-67, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_702() {
             Item[] items = new Item[] { new Item("Aged Brie", -42, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-43, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_703() {
             Item[] items = new Item[] { new Item("Aged Brie", -74, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-75, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_704() {
             Item[] items = new Item[] { new Item("Aged Brie", -58, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-59, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_705() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -14, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-15, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_706() {
             Item[] items = new Item[] { new Item("Aged Brie", -14, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-15, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_707() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -18, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-19, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_708() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -78, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-79, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_709() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -8, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_710() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -7, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-8, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_711() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -31, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-32, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_712() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -86, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-87, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_713() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 67, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_714() {
             Item[] items = new Item[] { new Item("Aged Brie", -14, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-15, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_715() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -87, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-88, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_716() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 36, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(35, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_717() {
             Item[] items = new Item[] { new Item("Aged Brie", -59, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-60, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_718() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -92, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-93, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_719() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 32, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(31, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_720() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 2, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(2, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_721() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 15, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(15, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_722() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -87, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-88, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_723() {
             Item[] items = new Item[] { new Item("Aged Brie", 17, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_724() {
             Item[] items = new Item[] { new Item("Aged Brie", 71, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(70, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_725() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 48, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(48, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_726() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -4, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-5, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_727() {
             Item[] items = new Item[] { new Item("Aged Brie", 34, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(33, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_728() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -73, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-74, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_729() {
             Item[] items = new Item[] { new Item("Aged Brie", -67, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-68, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_730() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 1, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_731() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 63, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(62, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_732() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -93, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-93, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_733() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -89, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-89, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_734() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 51, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_735() {
             Item[] items = new Item[] { new Item("Aged Brie", -14, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-15, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_736() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -83, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-83, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_737() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 65, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(65, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_738() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 46, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(45, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_739() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 95, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(94, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_740() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -99, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-100, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_741() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -4, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-5, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_742() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 74, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(73, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_743() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -12, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-13, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_744() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -33, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-34, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_745() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -12, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-13, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_746() {
             Item[] items = new Item[] { new Item("Aged Brie", 83, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(82, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_747() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 15, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(14, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_748() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -1, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-2, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_749() {
             Item[] items = new Item[] { new Item("Aged Brie", 93, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(92, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_750() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 4, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(3, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_751() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -34, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-35, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_752() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 29, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(28, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_753() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -85, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-85, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_754() {
             Item[] items = new Item[] { new Item("Aged Brie", -74, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-75, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_755() {
             Item[] items = new Item[] { new Item("Aged Brie", 69, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_756() {
             Item[] items = new Item[] { new Item("Aged Brie", -6, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_757() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -76, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_758() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 39, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_759() {
             Item[] items = new Item[] { new Item("Aged Brie", -20, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-21, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_760() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -8, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-8, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_761() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -25, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-26, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_762() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 71, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(70, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_763() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -6, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_764() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -22, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-23, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_765() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 7, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(6, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_766() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -56, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-57, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_767() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 80, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(79, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_768() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 95, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(94, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_769() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -93, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_770() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 99, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_771() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -89, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-90, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_772() {
             Item[] items = new Item[] { new Item("Aged Brie", 86, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_773() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -81, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-82, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_774() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -53, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-54, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_775() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -33, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-34, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_776() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -46, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-47, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_777() {
             Item[] items = new Item[] { new Item("Aged Brie", -98, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-99, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_778() {
             Item[] items = new Item[] { new Item("Aged Brie", 44, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(28, items[0].quality);
        }
        @Test
        void test_779() {
             Item[] items = new Item[] { new Item("Aged Brie", -27, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-28, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_780() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -1, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-2, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_781() {
             Item[] items = new Item[] { new Item("Aged Brie", 9, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(8, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_782() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 30, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(29, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_783() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -94, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_784() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -4, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_785() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -80, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-80, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_786() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 88, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(87, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_787() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 57, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(56, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_788() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -43, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-44, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_789() {
             Item[] items = new Item[] { new Item("Aged Brie", 44, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(43, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_790() {
             Item[] items = new Item[] { new Item("Aged Brie", 89, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_791() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 14, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(14, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_792() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -45, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-46, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_793() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 94, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(93, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_794() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 63, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(62, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_795() {
             Item[] items = new Item[] { new Item("Aged Brie", -72, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-73, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_796() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 65, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(64, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_797() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -10, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-11, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_798() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -1, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-2, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_799() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -12, 34) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-13, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_800() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 84, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(83, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_801() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 80, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(80, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_802() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -11, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-12, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_803() {
             Item[] items = new Item[] { new Item("Aged Brie", 37, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(36, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_804() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -23, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-24, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_805() {
             Item[] items = new Item[] { new Item("Aged Brie", -41, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-42, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_806() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -47, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-48, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_807() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -87, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-88, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_808() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -6, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-6, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_809() {
             Item[] items = new Item[] { new Item("Aged Brie", -13, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-14, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_810() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -49, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-50, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_811() {
             Item[] items = new Item[] { new Item("Aged Brie", -78, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-79, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_812() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -3, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_813() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -21, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-22, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_814() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 32, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(31, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_815() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -43, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-44, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_816() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -18, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-18, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_817() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 90, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(90, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_818() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -8, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_819() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 85, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_820() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -77, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_821() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -62, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-63, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_822() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 44, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(44, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_823() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 45, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(44, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_824() {
             Item[] items = new Item[] { new Item("Aged Brie", -96, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-97, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_825() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -29, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-30, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_826() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -95, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-96, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_827() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 15, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(14, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_828() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -49, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-50, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_829() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 60, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(60, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_830() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -9, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_831() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 21, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(20, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_832() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 13, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(13, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_833() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -20, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_834() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 55, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(54, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_835() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 42, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(42, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_836() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -61, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-61, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_837() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -50, 44) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-51, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_838() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 51, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_839() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 19, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(18, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_840() {
             Item[] items = new Item[] { new Item("Aged Brie", -32, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-33, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_841() {
             Item[] items = new Item[] { new Item("Aged Brie", 66, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(65, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_842() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -46, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-47, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_843() {
             Item[] items = new Item[] { new Item("Aged Brie", -75, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-76, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_844() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -68, 33) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-69, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_845() {
             Item[] items = new Item[] { new Item("Aged Brie", 2, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(1, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_846() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -19, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_847() {
             Item[] items = new Item[] { new Item("Aged Brie", -94, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_848() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 8, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(7, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_849() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 63, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(63, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_850() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -18, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-19, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_851() {
             Item[] items = new Item[] { new Item("Aged Brie", -31, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-32, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_852() {
             Item[] items = new Item[] { new Item("Aged Brie", 17, 15) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(16, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_853() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -61, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-61, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_854() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -73, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-73, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_855() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -4, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-5, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_856() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -18, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-19, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_857() {
             Item[] items = new Item[] { new Item("Aged Brie", 10, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(9, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_858() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 89, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_859() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 50, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_860() {
             Item[] items = new Item[] { new Item("Aged Brie", -35, 5) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-36, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_861() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 86, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_862() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 22, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_863() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 36, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(36, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_864() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 76, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_865() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -72, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-73, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_866() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 29, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(29, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_867() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 16, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(15, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_868() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -48, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-49, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_869() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -87, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-88, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_870() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 45, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(45, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_871() {
             Item[] items = new Item[] { new Item("Aged Brie", -83, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-84, items[0].sellIn);
            assertEquals(49, items[0].quality);
        }
        @Test
        void test_872() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 90, 37) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_873() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 3, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(2, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_874() {
             Item[] items = new Item[] { new Item("Aged Brie", -65, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-66, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_875() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 7, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(7, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_876() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 98, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_877() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -74, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-74, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_878() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -9, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-10, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_879() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -53, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-54, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_880() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 68, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(67, items[0].sellIn);
            assertEquals(12, items[0].quality);
        }
        @Test
        void test_881() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 80, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(80, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_882() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -78, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-78, items[0].sellIn);
            assertEquals(3, items[0].quality);
        }
        @Test
        void test_883() {
             Item[] items = new Item[] { new Item("Aged Brie", 99, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_884() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 99, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(99, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_885() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 67, 31) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_886() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 38, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(38, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_887() {
             Item[] items = new Item[] { new Item("Aged Brie", 40, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_888() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 90, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(2, items[0].quality);
        }
        @Test
        void test_889() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 1, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(0, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_890() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 79, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(79, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_891() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 62, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(61, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_892() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -21, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-22, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_893() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -14, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-14, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_894() {
             Item[] items = new Item[] { new Item("Aged Brie", 9, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(8, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_895() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -89, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-90, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_896() {
             Item[] items = new Item[] { new Item("Aged Brie", 16, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(15, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_897() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -44, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-44, items[0].sellIn);
            assertEquals(11, items[0].quality);
        }
        @Test
        void test_898() {
             Item[] items = new Item[] { new Item("Aged Brie", -49, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-50, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_899() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 89, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_900() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 89, 42) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(41, items[0].quality);
        }
        @Test
        void test_901() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -8, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_902() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 6, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(6, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_903() {
             Item[] items = new Item[] { new Item("Aged Brie", 6, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(5, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_904() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 28, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(27, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_905() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 97, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_906() {
             Item[] items = new Item[] { new Item("Aged Brie", -7, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-8, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_907() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 27, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(26, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_908() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -3, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_909() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 49, 21) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(49, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_910() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 76, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(75, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_911() {
             Item[] items = new Item[] { new Item("Aged Brie", -46, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-47, items[0].sellIn);
            assertEquals(37, items[0].quality);
        }
        @Test
        void test_912() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 22, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(21, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_913() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 77, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(76, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_914() {
             Item[] items = new Item[] { new Item("Aged Brie", 4, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(3, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_915() {
             Item[] items = new Item[] { new Item("Aged Brie", 86, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_916() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -87, 14) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-87, items[0].sellIn);
            assertEquals(14, items[0].quality);
        }
        @Test
        void test_917() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 42, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(42, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_918() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 69, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(68, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_919() {
             Item[] items = new Item[] { new Item("Aged Brie", -24, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-25, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_920() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -1, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-2, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_921() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -19, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-20, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_922() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -97, 7) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-98, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_923() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 55, 50) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(55, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_924() {
             Item[] items = new Item[] { new Item("Aged Brie", -88, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-89, items[0].sellIn);
            assertEquals(32, items[0].quality);
        }
        @Test
        void test_925() {
             Item[] items = new Item[] { new Item("Aged Brie", -58, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-59, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_926() {
             Item[] items = new Item[] { new Item("Aged Brie", -14, 13) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-15, items[0].sellIn);
            assertEquals(15, items[0].quality);
        }
        @Test
        void test_927() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 77, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(77, items[0].sellIn);
            assertEquals(16, items[0].quality);
        }
        @Test
        void test_928() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 95, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(95, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_929() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 64, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(63, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_930() {
             Item[] items = new Item[] { new Item("Aged Brie", -57, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-58, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_931() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 40, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(39, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_932() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -40, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-41, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_933() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 99, 49) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(50, items[0].quality);
        }
        @Test
        void test_934() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -98, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-99, items[0].sellIn);
            assertEquals(38, items[0].quality);
        }
        @Test
        void test_935() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 71, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(71, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_936() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 3, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(2, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_937() {
             Item[] items = new Item[] { new Item("Aged Brie", 60, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_938() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -94, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-94, items[0].sellIn);
            assertEquals(47, items[0].quality);
        }
        @Test
        void test_939() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 99, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(98, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_940() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -9, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_941() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -69, 47) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-70, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_942() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -94, 38) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-95, items[0].sellIn);
            assertEquals(36, items[0].quality);
        }
        @Test
        void test_943() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -78, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-79, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_944() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -9, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-10, items[0].sellIn);
            assertEquals(6, items[0].quality);
        }
        @Test
        void test_945() {
             Item[] items = new Item[] { new Item("Aged Brie", -3, 22) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_946() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 60, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_947() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -13, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-14, items[0].sellIn);
            assertEquals(8, items[0].quality);
        }
        @Test
        void test_948() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -38, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-38, items[0].sellIn);
            assertEquals(39, items[0].quality);
        }
        @Test
        void test_949() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 28, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(27, items[0].sellIn);
            assertEquals(9, items[0].quality);
        }
        @Test
        void test_950() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -4, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-4, items[0].sellIn);
            assertEquals(23, items[0].quality);
        }
        @Test
        void test_951() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 92, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(91, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_952() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 50, 25) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(50, items[0].sellIn);
            assertEquals(25, items[0].quality);
        }
        @Test
        void test_953() {
             Item[] items = new Item[] { new Item("Aged Brie", 60, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_954() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 22, 48) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(22, items[0].sellIn);
            assertEquals(48, items[0].quality);
        }
        @Test
        void test_955() {
             Item[] items = new Item[] { new Item("Aged Brie", 55, 41) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(54, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_956() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -60, 24) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-60, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_957() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 67, 30) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(66, items[0].sellIn);
            assertEquals(31, items[0].quality);
        }
        @Test
        void test_958() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 31, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(31, items[0].sellIn);
            assertEquals(1, items[0].quality);
        }
        @Test
        void test_959() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 59, 11) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(58, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_960() {
             Item[] items = new Item[] { new Item("Aged Brie", -40, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-41, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_961() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 35, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(34, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_962() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 75, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(74, items[0].sellIn);
            assertEquals(42, items[0].quality);
        }
        @Test
        void test_963() {
             Item[] items = new Item[] { new Item("Aged Brie", 90, 6) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(89, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_964() {
             Item[] items = new Item[] { new Item("Aged Brie", 60, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(59, items[0].sellIn);
            assertEquals(33, items[0].quality);
        }
        @Test
        void test_965() {
             Item[] items = new Item[] { new Item("Aged Brie", 97, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(96, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_966() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 69, 4) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(69, items[0].sellIn);
            assertEquals(4, items[0].quality);
        }
        @Test
        void test_967() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 64, 35) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(63, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_968() {
             Item[] items = new Item[] { new Item("Aged Brie", 19, 16) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(18, items[0].sellIn);
            assertEquals(17, items[0].quality);
        }
        @Test
        void test_969() {
             Item[] items = new Item[] { new Item("Aged Brie", -76, 3) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(5, items[0].quality);
        }
        @Test
        void test_970() {
             Item[] items = new Item[] { new Item("Aged Brie", 63, 12) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(62, items[0].sellIn);
            assertEquals(13, items[0].quality);
        }
        @Test
        void test_971() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -73, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-74, items[0].sellIn);
            assertEquals(26, items[0].quality);
        }
        @Test
        void test_972() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -100, 46) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-101, items[0].sellIn);
            assertEquals(44, items[0].quality);
        }
        @Test
        void test_973() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 81, 29) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(80, items[0].sellIn);
            assertEquals(30, items[0].quality);
        }
        @Test
        void test_974() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 89, 28) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(88, items[0].sellIn);
            assertEquals(29, items[0].quality);
        }
        @Test
        void test_975() {
             Item[] items = new Item[] { new Item("Aged Brie", 8, 45) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(7, items[0].sellIn);
            assertEquals(46, items[0].quality);
        }
        @Test
        void test_976() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 90, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(90, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_977() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 70, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(69, items[0].sellIn);
            assertEquals(22, items[0].quality);
        }
        @Test
        void test_978() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 66, 39) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(65, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_979() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 33, 23) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(32, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_980() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 46, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(45, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_981() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 52, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(52, items[0].sellIn);
            assertEquals(43, items[0].quality);
        }
        @Test
        void test_982() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -85, 2) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-86, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_983() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -76, 1) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-77, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_984() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -47, 0) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-48, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_985() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", -68, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-69, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_986() {
             Item[] items = new Item[] { new Item("Aged Brie", 86, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(19, items[0].quality);
        }
        @Test
        void test_987() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", -79, 26) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-80, items[0].sellIn);
            assertEquals(24, items[0].quality);
        }
        @Test
        void test_988() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 79, 9) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(78, items[0].sellIn);
            assertEquals(10, items[0].quality);
        }
        @Test
        void test_989() {
             Item[] items = new Item[] { new Item("Aged Brie", -41, 32) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-42, items[0].sellIn);
            assertEquals(34, items[0].quality);
        }
        @Test
        void test_990() {
             Item[] items = new Item[] { new Item("Aged Brie", 86, 20) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(85, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_991() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -79, 27) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-79, items[0].sellIn);
            assertEquals(27, items[0].quality);
        }
        @Test
        void test_992() {
             Item[] items = new Item[] { new Item("+5 Dexterity Vest", 92, 36) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(91, items[0].sellIn);
            assertEquals(35, items[0].quality);
        }
        @Test
        void test_993() {
             Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", -9, 40) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-9, items[0].sellIn);
            assertEquals(40, items[0].quality);
        }
        @Test
        void test_994() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 55, 17) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(54, items[0].sellIn);
            assertEquals(18, items[0].quality);
        }
        @Test
        void test_995() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -6, 18) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-7, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }
        @Test
        void test_996() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 6, 19) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(5, items[0].sellIn);
            assertEquals(21, items[0].quality);
        }
        @Test
        void test_997() {
             Item[] items = new Item[] { new Item("Elixir of the Mongoose", 52, 8) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(51, items[0].sellIn);
            assertEquals(7, items[0].quality);
        }
        @Test
        void test_998() {
             Item[] items = new Item[] { new Item("Aged Brie", -85, 43) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-86, items[0].sellIn);
            assertEquals(45, items[0].quality);
        }
        @Test
        void test_999() {
             Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", -33, 10) };
            final GildedRose app = new GildedRose(items);
            app.updateQuality();
            assertEquals(-34, items[0].sellIn);
            assertEquals(0, items[0].quality);
        }

}
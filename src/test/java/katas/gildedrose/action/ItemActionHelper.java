package katas.gildedrose.action;

import katas.gildedrose.Item;

public final class ItemActionHelper {

    public static int updateQuality(final int initial, ItemAction action) {
        final Item item = new Item("", 10, initial);
        action.apply(item);
        return item.quality;
    }
}

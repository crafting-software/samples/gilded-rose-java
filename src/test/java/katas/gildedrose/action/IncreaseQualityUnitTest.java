package katas.gildedrose.action;

import io.vavr.test.Property;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static io.vavr.test.Arbitrary.ofAll;
import static io.vavr.test.Gen.choose;
import static katas.gildedrose.action.ItemActionHelper.updateQuality;
import static katas.gildedrose.action.ItemActions.decreaseQuality;
import static katas.gildedrose.action.ItemActions.increaseQuality;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class IncreaseQualityUnitTest {

    @ParameterizedTest
    @ValueSource(ints = {-1, -7, -200})
    public void cannot_increase_quality_with_negative_values(final int amount) {
        assertThrows(IllegalArgumentException.class, () -> increaseQuality(amount));
    }

    @Test
    public void increase_quality_sample() {
        assertEquals(10, updateQuality(5, increaseQuality(5)));
        assertEquals(50, updateQuality(50, increaseQuality(5)));
        assertEquals(50, updateQuality(42, increaseQuality(8)));
    }

    @Test
    public void increase_quality_when_not_max() {
        Property.def("should increase quality when not max")
                .forAll(ofAll(choose(0, 49)))
                .suchThat((quality) -> updateQuality(quality, increaseQuality(1)) == quality + 1)
                .check()
                .assertIsSatisfied();
    }

    @Test
    public void increase_quality_invariants() {
        Property.def("increase quality should not go beyond 50")
                .forAll(ofAll(choose(0, 50)), ofAll(choose(0, 200)))
                .suchThat((initial, amount) -> updateQuality(initial, increaseQuality(amount)) <= 50)
                .check()
                .assertIsSatisfied();

        Property.def("increase quality should not go below 50")
                .forAll(ofAll(choose(0, 50)), ofAll(choose(0, 200)))
                .suchThat((initial, amount) -> updateQuality(initial, increaseQuality(amount)) >= 0)
                .check()
                .assertIsSatisfied();

        Property.def("increase quality should be greater or equal the initial quality")
                .forAll(ofAll(choose(0, 50)), ofAll(choose(0, 200)))
                .suchThat((initial, amount) -> updateQuality(initial, increaseQuality(amount)) >= initial)
                .check()
                .assertIsSatisfied();
    }
}

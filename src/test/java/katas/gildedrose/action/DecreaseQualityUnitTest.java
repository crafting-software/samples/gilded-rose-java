package katas.gildedrose.action;

import io.vavr.test.Property;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static io.vavr.test.Arbitrary.ofAll;
import static io.vavr.test.Gen.choose;
import static katas.gildedrose.action.ItemActionHelper.updateQuality;
import static katas.gildedrose.action.ItemActions.decreaseQuality;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DecreaseQualityUnitTest {

    @ParameterizedTest
    @ValueSource(ints = {-1, -7, -200})
    public void cannot_decrease_quality_with_negative_values(final int amount) {
        assertThrows(IllegalArgumentException.class, () -> decreaseQuality(amount));
    }

    @Test
    public void decrease_quality_sample() {
        assertEquals(10, updateQuality(15, decreaseQuality(5)));
        assertEquals(0, updateQuality(0, decreaseQuality(5)));
        assertEquals(34, updateQuality(42, decreaseQuality(8)));
    }

    @Test
    public void decrease_quality_when_not_max() {
        Property.def("should decrease quality when not min")
                .forAll(ofAll(choose(1, 50)))
                .suchThat((quality) -> updateQuality(quality, decreaseQuality(1)) == quality - 1)
                .check()
                .assertIsSatisfied();
    }

    @Test
    public void decrease_quality_invariants() {
        Property.def("decrease quality should not go beyond 50")
                .forAll(ofAll(choose(0, 50)), ofAll(choose(0, 200)))
                .suchThat((initial, amount) -> updateQuality(initial, decreaseQuality(amount)) <= 50)
                .check()
                .assertIsSatisfied();

        Property.def("decrease quality should not go below 50")
                .forAll(ofAll(choose(0, 50)), ofAll(choose(0, 200)))
                .suchThat((initial, amount) -> updateQuality(initial, decreaseQuality(amount)) >= 0)
                .check()
                .assertIsSatisfied();

        Property.def("decrease quality should be less or equal the initial quality")
                .forAll(ofAll(choose(0, 50)), ofAll(choose(0, 200)))
                .suchThat((initial, amount) -> updateQuality(initial, decreaseQuality(amount)) <= initial)
                .check()
                .assertIsSatisfied();
    }
}
